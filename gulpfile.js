/*
*Gulp.js настройки
*/ 
'use strict';
 
const
  // исходная папка и папка билда
  dir = {
    src         : 'src/',
    build       : 'dist'
  },
  // Gulp и плагины
  gulp          = require('gulp'),
  gutil         = require('gulp-util'),
  newer         = require('gulp-newer'),
  imagemin      = require('gulp-imagemin'),
  sass          = require('gulp-sass'),
  postcss       = require('gulp-postcss'),
  deporder      = require('gulp-deporder'),
  concat        = require('gulp-concat'),
  stripdebug    = require('gulp-strip-debug'),
  uglify        = require('gulp-uglify')
;
// PHP-настройки
const php = {
  src           : dir.src + 'template/**/*.php',
  build         : dir.build
};
// HTML-настройки
const html = {
  src           : dir.src + 'template/**/*.html',
  build         : dir.build
};
// Browser-sync
//var browsersync = false;
var browserSync = require('browser-sync');
var reload = browserSync.reload;


gulp.task('browserSync', function() {
  var files = [
    'dist/**/*.html',
    'dist/assets/css/**/*.css',
    'dist/assets/imgs/**/*.png',
    'dist/assets/js/**/*.js'
  ];
  browserSync.init(files,{
    server: {
      baseDir: "./dist"
    },
    port: 8080,
    open: true,
    notify: true
  });
});


/*
* Tasks
*/
gulp.task('build', ['php', 'css', 'js', 'html']);
gulp.task('default', ['build', 'watch']);
gulp.task('watch', ['browserSync'], () => {
  // изменения страниц
  gulp.watch(php.src, ['php'], browserSync ? browserSync.reload : {});
  gulp.watch(html.src, ['html']);
  // изменения изображений
  gulp.watch(images.src, ['images']);
  // CSS-изменения
  gulp.watch(css.watch, ['css']);
  // основные изменения в JavaScript
  gulp.watch(js.src, ['js']);
});

// копирование PHP-файлов
gulp.task('php', () => {
  return gulp.src(php.src)
    .pipe(newer(php.build))
    .pipe(gulp.dest(php.build));
});
// копирование HTML-файлов
gulp.task('html', () => {
  return gulp.src(html.src)
    .pipe(newer(html.build))
    .pipe(gulp.dest(html.build))
    .pipe(browserSync ? browserSync.reload({ stream: true }) : gutil.noop());
});
// настройки изображений
const images = {
    src         : dir.src + 'images/**/*',
    build       : dir.build + 'images/'
};
// обработка изображений
gulp.task('images', () => {
  return gulp.src(images.src)
    .pipe(newer(images.build))
    .pipe(imagemin())
    .pipe(gulp.dest(images.build));
});

// CSS-настройки
var css = {
    src         : dir.src + 'scss/style.scss',
    watch       : dir.src + 'scss/**/*',
    build       : dir.build,
    sassOpts: {
      outputStyle     : 'nested',
      imagePath       : images.build,
      precision       : 3,
      errLogToConsole : true
    },
    processors: [
      require('postcss-assets')({
        loadPaths: ['images/'],
        basePath: dir.build,
        baseUrl: 'dist'
      }),
      require('autoprefixer')({
        browsers: ['last 2 versions', '> 2%']
      }),
      require('css-mqpacker'),
      require('cssnano')
    ]
  };
   
// обработка CSS
gulp.task('css', () => {
    return gulp.src(css.src)
      .pipe(sass(css.sassOpts))
      .pipe(postcss(css.processors))
      .pipe(gulp.dest(css.build))
      .pipe(browserSync ? browserSync.reload({ stream: true }) : gutil.noop());
});

// JavaScript-настройки
const js = {
    src         : dir.src + 'js/**/*',
    build       : dir.build + '/js',
    filename    : 'app.js'
  };
// обработка JavaScript
gulp.task('js', () => {
  return gulp.src(js.src)
    .pipe(deporder())
    .pipe(concat(js.filename))
    .pipe(stripdebug())
    .pipe(uglify())
    .pipe(gulp.dest(js.build))
    .pipe(browserSync ? browserSync.reload({ stream: true }) : gutil.noop());
});

  

